import Pyro.core
from contato import *

# you have to change the URI below to match your own host/port.
agenda = Pyro.core.getProxyForURI("PYROLOC://localhost:7766/agenda")

while True:
    print "O que voce quer fazer, man?"
    print "1 = Inserir novo contato"
    print "2 = Buscar contato"

    opcao = int(raw_input(""))

    if opcao == 1:
        nome = raw_input("Digite o nome do novo contato: ")
        numero = raw_input("Digite o numero do novo contato: ")
        c = Contato(nome, numero)
        agenda.adicionaContato(c)
        print "\nContato adicionado com sucesso\n"
    elif opcao == 2:
        nome = raw_input("\nDigite o nome a ser buscado, magnatinha: ")
        contato = agenda.buscaContato(nome)
        if contato:
            print "\n%s %s\n" % (contato.getNome(), contato.getNumero())
        else:
            print "Nao encontrado\n"


