from contato import *
import Pyro.core

class Agenda(Pyro.core.ObjBase):
    def __init__(self):
        super(Agenda, self).__init__()
        self.listaContatos = []

    def adicionaContato(self, contato):
        self.listaContatos.append(contato)

    def escreveContatos(self):
        for contato in self.listaContatos:
            print contato.getNome() + ' ' + contato.getNumero()
    
    def buscaContato(self, nome):
        for contato in self.listaContatos:
            if contato.getNome() == nome:
                return contato
                
        return False

def main():
    agenda = Agenda()

    agenda.adicionaContato(Contato("Jack White","3224 2233"))
    agenda.adicionaContato(Contato("Angus Young","3225 1111"))
    agenda.adicionaContato(Contato("Brandon Flowers","3225 1111"))

    Pyro.core.initServer()
    daemon=Pyro.core.Daemon()
    uri=daemon.connect(agenda, "agenda")

    print "The daemon runs on port:",daemon.port
    print "The object's uri is:",uri

    daemon.requestLoop()
    #agenda.escreveContatos();

    #contato = agenda.buscaContato("Jack White")
    #if contato:
    # 	print "%s %s" % (contato.getNome(), contato.getNumero())
    #else:
    #    print "Nao encontrado"
	
if __name__ == "__main__":
    main()
